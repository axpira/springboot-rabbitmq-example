package br.com.thiagogbferreira;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.UUID;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

@SpringBootApplication
@RestController
public class SpringBootRabbitMqExampleApplication {
  @Autowired
  private RabbitTemplate rabbitTemplate;
  @Value("${exchange.name:EX.Test}" )
  public String exchange;
  
  public static void main(String[] args) {
    SpringApplication.run(SpringBootRabbitMqExampleApplication.class, args);
  }
  
  @PostMapping(value="/post", consumes="text/plain")
  public void post(@RequestBody String content) throws UnsupportedEncodingException, JsonProcessingException {
    MessageProperties prop = new MessageProperties();
    prop.setContentEncoding("UTF-8");
    prop.setMessageId(UUID.randomUUID().toString());
    prop.setContentType(MessageProperties.CONTENT_TYPE_TEXT_PLAIN);
    Message message = new Message(content.getBytes("UTF-8"), prop);
    rabbitTemplate.send(exchange, "", message);
  }

  @RabbitListener(queues = "${queue.name:QU.Test}" )
  public void receiveMessage(Message message, Channel channel) throws IOException {
    System.out.println("Mensagem recebida: " + new String((message.getBody())));
  }

  
}
