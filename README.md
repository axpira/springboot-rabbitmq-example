

Projeto criado para demonstrar como postar e ler mensagens do RabbitMQ

Esta sendo exposto um serviço REST que ao receber um POST posta a mensagem enviada na fila
E outro método que fica ouvindo a fila e quando receber uma mensagem na fila joga para o sysout a mensagem



# Changelog
* 1.0.0 - Versão Inicial


# Exemplo

Chamar a api de post para postar mensagem na fila
```
 curl -d 'teste' -H "Content-Type: application/json" http://localhost:8080/post
```
